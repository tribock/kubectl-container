# kubectl container

this Container is meant to be used in deployment steps which use additional tools to alpine

contains:
- kubectl
- envsubst
- bash
- python3
- git
- gcc
- gcloud cli

## usage

```bash
docker pull tribock/kubectl
```

### kubectl

```bash
NS=`echo ${k8s_namespace} | sed 's/\-/_/g' |  tr [a-z] [A-Z]`
export KUBECONFIGFILE=$(eval "echo \"KUBECONFIGFILE_\$NS\"")
echo "${!KUBECONFIGFILE}" > /config/.kube/config
cat /config/.kube/config
```

### envsubst

```bash
#!/bin/bash
export CI_PROJECT_NAMESPACE=${CI_PROJECT_NAMESPACE}
export CI_PROJECT_NAME=${CI_PROJECT_NAME}
export TAG_VAR=${TAG_VAR}
export K8S_NAMESPACE=${K8S_NAMESPACE}
export BASE_URL=${BASE_URL}
export CI_ENVIRONMENT_SLUG=${CI_ENVIRONMENT_SLUG}
export SUBGROUP=${SUBGROUP}
export IMAGE_NAME=${IMAGE_NAME}
export CI_COMMIT_TAG=${CI_COMMIT_TAG}

for f in /deploy/templates/*.yml
do
 FILENAME=$(basename ${f/.*/})
    if [[ ! -d /deploy/generated ]]; then
        mkdir -p /deploy/generated/
    fi
    envsubst < $f > "/deploy/generated/$(basename $f)"
    echo
done
```

### deploy

```bash
kubectl --kubeconfig /config/.kube/config apply -f /deploy/generated/
```