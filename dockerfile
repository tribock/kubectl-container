FROM google/cloud-sdk:alpine

ENV KUBECTLVERSION=v1.15.0

RUN wget https://storage.googleapis.com/kubernetes-release/release/"$KUBECTLVERSION"/bin/linux/amd64/kubectl &&\
    chmod +rx kubectl && mv kubectl /usr/local/bin/ && mkdir -p /config/.kube &&\
    apk add gettext --update && apk add bash curl --update && rm -rf /var/cache/apk/*  &&\
    apk add go git gcc libc-dev libltdl libtool libgcc python3 --update && \
    export GOPATH=/go 

RUN apk add --no-cache --virtual .build-deps g++ python3-dev libffi-dev openssl-dev &&\
    apk add --no-cache --update python3 &&     pip3 install --upgrade pip setuptools && pip install pyYaml
    